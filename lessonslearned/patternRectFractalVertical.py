#!/bin/python

import numpy as np
import matplotlib.pylab as plt





def draw_FractalFieldRectangular(nFractals, nFeatures, ySizeFractal, grayMin=0, grayMax=255, nColsSpacer=0, orient='up'):


    ## Iterables 

    grayShades = np.linspace(grayMax, grayMin, nFeatures)



    ## Containers

    features = []



    ## Create the Field

    for grayShade in grayShades[1:]:
        nColsFeature = 2**(nFractals-1)
        nRowsFeature = ySizeFractal*nFractals

        feature = np.ones((nRowsFeature,nColsFeature))*grayShades[0] #Array for feature

        spacer = np.ones((nRowsFeature,nColsSpacer))*grayShades[0]   #Spacer between features

        cFractal=0         #Counter of the fractals in the feature



        ### Create the Feature

        for cRowsFeature in range(nRowsFeature):
        
            if (cRowsFeature + 1) % ySizeFractal == 0:
        
                cFractal += 1
        
            for cColsFeature in range(nColsFeature):
            
                if cColsFeature < 2**cFractal:

                    feature[cRowsFeature,cColsFeature] = int(grayShade)

        if orient == 'down':
            features.append(np.flipud(feature))
        else:
            features.append(feature)

        features.append(spacer)

    return np.hstack(features)




if __name__ == "__main__":
    
    
    ## Parameters
    nFractals=5     #Number of fractal repetition in feature
    
    nFeatures=2
    
    ySizeFractal=10    #Vertical dimension 
    
    (grayMin, grayMax) = 0, 255     #Grayscale range
    
    nColsSpacer = 5     #Horizontal dimension of spacer between features
    
    orient = 'down'    #smallest feature pointing.. "up", "down"
    
    out = draw_FractalFieldRectangular(nFractals, nFeatures, ySizeFractal, grayMin=0, grayMax=255, nColsSpacer=0, orient='up')
    
    plt.matshow(out)
    plt.show()