import numpy as np
import cv2
import matplotlib.pylab as plt

class Field:

    def __init__(self,height_px,width_px):
        self.width_px=width_px
        self.height_px=height_px
        self.field=np.empty((height_px,width_px),np.uint8)
        self.name = ''

    def set_field_name_from_specs(self,**kwargs):
        l = ['{:s}{:d}'.format(key,kwargs[key]) for key in kwargs.keys()]
        self.name = 'field_'+'_'.join(l)

    def get_field_name(self):
        return self.name

    def set_field_background_color(self,color):
        self.field[:] = color
        return 0

    def save_field(self,filename):
        cv2.imwrite(filename,self.field)
        return 0

    # Draw functions
    def draw_line_horizontal(self,y,width,color=0):
        """
        Draw a horizontal line across the field
        :param y: vertical position
        :param width: width of the line
        :param color: (optional) color of the line
        :return: 0
        """
        self.field[y:y+width,:] = color
        return 0

    def draw_line_vertical(self,x,width,color=0):
        """
        Draw a verticla line across the field
        :param x: horizontal position
        :param width: width of the line
        :param color: (optional) color of the line
        :return: 0
        """
        self.field[:,x:x+width] = color
        return 0

    def draw_rectangle(self,pos,height,width,color=0):
        """
        Draw a rectangle in the field
        :param pos: tuple of bottom left corner position
        :param height: height
        :param width: width
        :param color: (optional) color
        :return: 0
        """
        y,x = pos
        self.field[y:y+height,x:x+width] = color
        return 0

    def draw_point(self,pos,color=0):
        """
        Draw a point
        :param pos: tuple of vertical and horizontal position
        :param color: (optional) color
        :return:
        """
        y,x = pos
        self.field[y,x] = color
        return 0

    def draw_circle(self,pos,radius,color=0):
        y0,x0 = pos
        for y in range(-radius, radius):
            for x in range(-radius, radius):
                if (x)**2+(y)**2 < radius**2:
                    self.field[y0+y,x0+x] = color
        return 0

    def draw_arc(self,pos,radius,thickness,color=0):
        y0,x0 = pos
        for y in range(0,self.height_px):
            for x in range(0,self.width_px):
                if (x-x0)**2+(y-y0)**2 >= (radius)**2 and (x-x0)**2+(y-y0)**2 <= (radius+thickness)**2 :
                    self.field[y,x] = color
        return 0

    def draw_corner(self,pos,thickness,orientation='nw',color=0):
        y0,x0 = pos
        if orientation == 'ne':
            self.field[0:y0,x0:x0+thickness] = color
            self.field[y0-thickness:y0,x0:self.width_px] = color
        elif orientation == 'se':
            self.field[y0:y0+thickness,x0:x0+self.width_px] = color
            self.field[y0:y0+self.height_px,x0:x0+thickness] = color
        elif orientation == 'sw':
            self.field[y0:y0+thickness,0:x0] = color
            self.field[y0:self.height_px,x0-thickness:x0] = color
        elif orientation == 'nw':
            self.field[y0-thickness:y0,0:x0] = color
            self.field[0:y0,x0-thickness:x0] = color
        else:
            NameError("Please enter valid orientation 'ne','se','sw','ne'")
        return 0


    # Fill Field with pattern methods
    def fill_with_circles_cubic(self,radius,distance,color):
        for i in range(distance,self.height_px-distance+1,distance):
            for j in range(distance,self.width_px-distance+1,distance):
                print(i,j)
                self.draw_circle((i,j),radius,color=color)
        return 0

    def fill_with_circles_hexagonal(self,radius,distance,color):
        for offset in [0, distance//2]:
            for i in range(distance+offset,self.height_px-distance+1,distance):
                for j in range(distance+offset,self.width_px-distance+1,distance):
                    self.draw_circle((i,j),radius,color=color)
        return 0

    def fill_with_corners(self, ncorners, thickness, distance, orientation, color):
        period=thickness+distance
        if orientation == 'ne':
            x0 = range(0, (ncorners) * period, period)
            y0 = range(self.height_px , self.height_px - (ncorners) * period, -period)
        elif orientation == 'se':
            x0 = range(0,(ncorners)*period,period)
            y0 = range(0,(ncorners)*period,period)
        elif orientation == 'sw':
            x0 = range(self.width_px,self.width_px-(ncorners)*period,-period)
            y0 = range(0,(ncorners)*period,period)
        elif orientation == 'nw':
            x0 = range(self.width_px, self.width_px - (ncorners) * period, -period)
            y0 = range(self.height_px, self.height_px - (ncorners) * period, -period)
        else:
            NameError("Please enter valid orientation: 'ne','se','nw','sw'")
        for i,(y,x) in enumerate(zip(y0,x0)):
            self.draw_corner((y,x),thickness, orientation, color)
        return 0

    # Plot
    def plot_field(self,ax=None):
        if ax == None:
            fig,ax = plt.subplots(1,1)
        ax.matshow(self.field,cmap='gray')
        return ax

def concatenate_fields(fields):
    # concatenate fields
    rows = []
    print(len(fields))
    for row in fields:
        rows.append(np.concatenate(row, axis=1))
    if len(fields)>1:
        temp = np.concatenate(rows, axis=0)
    else:
        temp = rows[0]
    height, width = temp.shape
    field = Field(height,width)
    field.field = temp
    return field

def make_2x2_corner_field(size,ncorners,thickness,distance):

    h_px, w_px = size
    arc_radius = h_px//4
    arc_thickness = thickness

    # nw
    nw = Field(h_px, w_px)
    nw.set_field_background_color(255)
    nw.fill_with_corners(ncorners,thickness,distance, 'nw', 0)
    [nw.draw_arc((0, 0), arc_radius+arc_thickness*i, arc_thickness*2, 0) for i in range(0,2*ncorners,4)]

    # ne
    ne = Field(h_px,w_px)
    ne.set_field_background_color(0)
    ne.fill_with_corners(ncorners//2,thickness*2,distance*2,'ne',255)
    [ne.draw_arc((0, w_px), arc_radius+arc_thickness*i, arc_thickness, 255) for i in range(0,2*ncorners,2)]

    # sw
    sw = Field(h_px,w_px)
    sw.set_field_background_color(0)
    sw.fill_with_corners(ncorners//2,thickness*2,distance*2,'sw',255)
    [sw.draw_arc((h_px, 0), arc_radius+arc_thickness*i, arc_thickness, 255) for i in range(0,2*ncorners,2)]

    # se
    se = Field(h_px,w_px)
    se.set_field_background_color(255)
    se.fill_with_corners(ncorners,thickness,distance,'se',0)
    [se.draw_arc((h_px, w_px), arc_radius+arc_thickness*i, arc_thickness*2, 0) for i in range(0,2*ncorners,4)]

    # Concatenate fields
    field = concatenate_fields([[se.field,sw.field],[ne.field,nw.field]])

    return field


def make_2x2_indent_field(size,radius_small, radius_large, distance):

    h_px, w_px = size

    # ne
    ne = Field(h_px, w_px)
    ne.set_field_background_color(0)
    ne.fill_with_circles_cubic(radius_small, distance, 255)
    # se
    se = Field(h_px, w_px)
    se.set_field_background_color(255)
    se.fill_with_circles_cubic(radius_small, distance, 0)
    # sw
    sw = Field(h_px, w_px)
    sw.set_field_background_color(0)
    sw.fill_with_circles_cubic(radius_large, distance, 255)
    # nw
    nw = Field(h_px, w_px)
    nw.set_field_background_color(255)
    nw.fill_with_circles_cubic(radius_large, distance, 0)

    # Concatenate fields
    field = concatenate_fields([[se.field, ne.field], [sw.field, nw.field]])

    return field


def make_greyscale_checkerboard(size,nsquares):
    h_px, w_px = size
    n=int(np.sqrt(nsquares))
    h_field = h_px//n
    w_field = w_px//n
    h_px = h_field*n
    w_px = w_field*n

    field = Field(h_px,w_px)

    color_step = int(256/nsquares)
    colors = np.arange(0,256,color_step)
    print(colors)
    np.random.shuffle(colors)

    i_color = 0
    for y in range(0,h_px,h_field):
        for x in range(0,w_px,w_field):
            field.draw_rectangle((y,x),h_field,w_field,color=colors[i_color])
            i_color+=1

    return field

def make_development_test_pattern(fieldSize,squareSize,squareDistance):
    """
    Create a pattern with n squares to test the development of silk
    :param size: (height, width) of the field in px
    :param nfeatures: number of fetures in y and x direction(square number)
    :return: field
    """

    field = Field(fieldSize,fieldSize)
    field.set_field_background_color(255)
    field.set_field_name_from_specs(fSize=fieldSize,sqSize=squareSize,sqDist=squareDistance)

    for y0 in range(squareDistance,fieldSize-squareDistance,squareSize+squareDistance):
        for x0 in range(squareDistance,fieldSize-squareDistance,squareSize+squareDistance):
            field.draw_rectangle((y0,x0),squareSize,squareSize,0)

    return field

def make_line_edge_test_pattern(size,orientation,thickness,distance):
    h_px, w_px = size

    # White background with black features
    field_wb = Field(h_px,w_px)
    field_wb.set_field_background_color(255)
    if orientation == 'vertical':
        for x in range(distance,w_px-distance,thickness+distance):
            field_wb.draw_line_vertical(x, thickness, 0)
    elif orientation == 'horizontal':
        for y in range(distance,h_px-distance,thickness+distance):
            field_wb.draw_line_horizontal(y, thickness, 0)
    else:
        NameError("Please choose valid orientation: 'vertical' or 'horizontal'")

    # black background with white features
    field_bw = Field(h_px, w_px)
    field_bw.set_field_background_color(0)
    if orientation == 'vertical':
        for x in range(distance, w_px - distance, thickness + distance):
            field_bw.draw_line_vertical(x, thickness, 255)
    elif orientation == 'horizontal':
        for y in range(distance, h_px - distance, thickness + distance):
            field_bw.draw_line_horizontal(y, thickness, 255)
    else:
        NameError("Please choose valid orientation: 'vertical' or 'horizontal'")

    field = concatenate_fields([[field_bw.field, field_wb.field]])
    field.set_field_name_from_specs(height=size[0],width=size[1],thick=thickness,dist=distance)

    return field
print('hello')
if __name__ == '__main__':

    print('hello main')

    # Corner Field normal & inverse
    w_px = 200
    h_px = 200
    ncorners = 10
    thickness = 5
    distance = 5
    field = make_2x2_corner_field((h_px,w_px),ncorners,thickness,distance)
    field.plot_field()
    field.save_field('corner13.png')

    print('indent')
    # Indent Field normal & inverse
    w_px = 100
    h_px = 100
    radius_small = 2
    radius_large = 4
    distance = 10
    field = make_2x2_indent_field((h_px,w_px), radius_small, radius_large, distance)
    field.plot_field()
    field.save_field('dots12.png')

    print('checkerboard')
    # Checkerboard
    w_px = 1000
    h_px = 1000
    ncolors=49
    field = make_greyscale_checkerboard((h_px,w_px),ncolors)
    field.plot_field()
    field.save_field('checkerboard01.png')

    print('development')
    # Development test pattern
    fieldSize = 140
    squareSize = 20
    squareDistance = 20
    field = make_development_test_pattern(fieldSize,squareSize,squareDistance)
    field.plot_field()
    field.save_field(field.get_field_name()+'.png')


    # Line Edge Roughness
    w_px = 125
    h_px = 250
    orientation = 'horizontal'
    thickness = 2
    distance = 50-thickness
    field = make_line_edge_test_pattern((h_px,w_px),orientation,thickness,distance)
    field.plot_field()
    field.save_field(field.get_field_name()+'.png')

    plt.show()
