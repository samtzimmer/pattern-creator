# Bitmap Resolution Pattern Creator for Lithography

This project contains python code that can be used create a set of test patterns for measuring the resolution of a lithography technique. It can create simple patterns such as Stripes, Siemens stars, Checkerboard patterns, Fractal patterns and many more that can be easily together to form complex test patterns.

## Getting Started

### Prerequisites

1. Python >3.5
2. Opencv

### Installing

1. Clone the project into your local directory
2. Run main.py file
3. Create you own test patterns in main.py

### Examples
