#!/usr/bin/env python3
"""
Copyright (c) (2019) Samuel Zimmermann <samuel.zimmermann@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import matplotlib.pyplot as plt
import patterns 


def test_pattern_20190704():
    """Creates a test pattern with various elements to measure the resolution of the technique
    """
    
    rt = patterns.ResolutionTest(2000, 700)
    
    xBorder = 280
    yBorder = 80
    
    # add horizontal stripes
    rt.add_pattern(xBorder, yBorder,  patterns.Stripes(100, 100, 1, 4, 'horizontal'))
    rt.add_pattern(200+xBorder, yBorder,  patterns.Stripes(100, 100, 1, 6, 'horizontal'))
    rt.add_pattern(400+xBorder, yBorder,  patterns.Stripes(100, 100, 1, 8, 'horizontal'))
    rt.add_pattern(600+xBorder, yBorder,  patterns.Stripes(100, 100, 1, 10, 'horizontal'))

    # add vertical stripes
    rt.add_pattern(xBorder, 200+yBorder,  patterns.Stripes(100, 100, 1, 4, 'vertical'))
    rt.add_pattern(200+xBorder, 200+yBorder,  patterns.Stripes(100, 100, 1, 6, 'vertical'))
    rt.add_pattern(400+xBorder, 200+yBorder,  patterns.Stripes(100, 100, 1, 8, 'vertical'))    
    rt.add_pattern(600+xBorder, 200+yBorder,  patterns.Stripes(100, 100, 1, 10, 'vertical'))    

    # add checkerboards
    rt.add_pattern(xBorder, 400+yBorder, patterns.Checkerboard(160, 160, 10))
    rt.add_pattern(xBorder+(710-160)//2, 400+yBorder, patterns.Checkerboard(160, 160, 20))
    rt.add_pattern(710+xBorder-160, 400+yBorder, patterns.Checkerboard(160, 160, 40))
    
    # add fractal stripes
    fs = patterns.FractalStripes(240,240,1,50)
    rt.add_pattern(xBorder+850, yBorder, fs)
    fs.rotate('cw')
    rt.add_pattern(xBorder+850, yBorder+300, fs)
    
    # add circles
    fc = patterns.FractalCircles(120,200,1,2,6)
    rt.add_pattern(xBorder+1200, yBorder, fc)
    fc.invert_color()
    fc.flip_ud()
    rt.add_pattern(xBorder+1200+120, yBorder, fc)
    
    # add corners
    fc = patterns.Corners(120, 120, 1, 10, 120, 90, 10)
    rt.add_pattern(xBorder+1200, yBorder+440, fc)
    rt.add_pattern(xBorder+1320, yBorder+440, fc.invert_color().rotate('cw'))
    
    fc = patterns.Corners(120, 120, 1, 8, 120, 90, 10)
    rt.add_pattern(xBorder+1200, yBorder+300, fc)
    rt.add_pattern(xBorder+1320, yBorder+300, fc.invert_color().rotate('cw'))    
    
    # add markers
    rt.draw_line_markers(2, 100)
    
    return rt


def compact_pattern_20190724():
    """Creates a compact test pattern with various elements to measure the resolution of the technique
    """
       
    width_px, height_px = 1500, 900
    
    xBorder_px, yBorder_px = 80, 40
    
    boxSize_px = 240
    
    
    # box offset
    boxOffset_px = int(boxSize_px*1.2)
    
    # create test pattern
    rt = patterns.ResolutionTest(width_px, height_px)
    
    # set coordinates    
    x0, y0 = (xBorder_px, yBorder_px)
    gap = boxOffset_px
    
    # add horizontal stripes
    rt.add_pattern(x0, y0,  patterns.StripesDouble(boxSize_px, boxSize_px, 1, 6, 'horizontal'))
    rt.add_pattern(x0+gap, y0,  patterns.StripesDouble(boxSize_px, boxSize_px, 1, 8, 'horizontal'))
    
    # add vertical stripes
    rt.add_pattern(x0, y0+gap,  patterns.StripesDouble(boxSize_px, boxSize_px, 1, 6, 'vertical'))
    rt.add_pattern(x0+gap, y0+gap,  patterns.StripesDouble(boxSize_px, boxSize_px, 1, 8, 'vertical'))
    
    # set coordinates
    x0, y0 = (x0, y0+gap*2)
    
    # add checkerboards
    rt.add_pattern(x0, y0, patterns.Checkerboard(boxSize_px, boxSize_px, 10))
    rt.add_pattern(x0+gap, y0, patterns.Checkerboard(boxSize_px, boxSize_px, 15))
    
    # set coordinates
    x0, y0 = (x0+2*gap, yBorder_px)
    
    # add siemensstars
    rt.add_pattern(x0, y0, patterns.SiemensStar(boxSize_px, boxSize_px, int(boxSize_px//2), 4))
    rt.add_pattern(x0, y0+gap, patterns.SiemensStar(boxSize_px, boxSize_px, int(boxSize_px//2), 8))
    rt.add_pattern(x0, y0+2*gap, patterns.SiemensStar(boxSize_px, boxSize_px, int(boxSize_px//2), 16))
    
    # set coordinates
    x0, y0 = (x0+gap, yBorder_px)
    
    # add corners
    fc = patterns.Corners(boxSize_px, boxSize_px, 3, 13, boxSize_px, int(boxSize_px*.7), 13)
    rt.add_pattern(x0, y0, fc)
    rt.add_pattern(x0+boxSize_px, y0, fc.invert_color().rotate('cw'))
    

    # set coordinates
    x0, y0 = (x0, y0+gap)
    
    # add fractal Circles
    fc = patterns.FractalCircles(boxSize_px, boxSize_px+gap,1,1.85,8)
    rt.add_pattern(x0, y0, fc)
    fc.invert_color()
    fc.flip_ud()
    rt.add_pattern(x0+boxSize_px, y0, fc)

    # add markers
    rt.draw_line_markers(2, 20)
    
    return rt

if __name__ == '__main__':
    #    import doctest
#    doctest.testmod()

    rt = compact_pattern_20190724()
    
    # plotting
    rt.plot()
    plt.show()
    
    rt.save_png('../img/compactTestPattern_{:d}x{:d}.png'.format(rt.canvasWidth, rt.canvasHeight))