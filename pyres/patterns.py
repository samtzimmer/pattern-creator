#!/usr/bin/env python3
"""
Copyright (c) (2019) Samuel Zimmermann <samuel.zimmermann@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import colortools as ct
import matplotlib.pylab as plt
import cv2

class Pattern:
    """Main pattern class shared by all patterns
    
    >>> p = Pattern('test', 3, 3)
    >>> p.name
    'test'
    
    """
    
    def __init__(self, name, canvasWidth, canvasHeight, canvasColor='white'):
        """Creates instance of the pattern class
        
        :param name: Direction of the rotation either counterclockwise (ccw) or clockwise (cw)
        :type name: str
        :param canvasWidth: width of the pattern canvas
        :type canvasWidth: int
        :param cavasHeight: height of the pattern canvas
        :type canvasHeight: int
        :param canvasColor: color of the pattern canvas
        :type canvasColor: str, int
        :returns: None
        
        """ 
        # check if name is a string
        if (isinstance(name, str)):
            self.name = name #Name of the pattern        
        else:
            raise Exception('name must be a string.')
            
        # check that canvas width and height are integers
        if (isinstance(canvasWidth, int)) & (isinstance(canvasHeight, int)):
            self.canvasWidth = canvasWidth #Width of the canvas
            self.canvasHeight = canvasHeight #Height of the canvals
        else:
            raise Exception('canvasHeight and canvasWidth must be integers.')
        
        # check if color name is valid:
        if (canvasColor in ct.valid_color_names) or (isinstance(canvasColor, int) and 0 <= canvasColor <= 255):
            self.canvasColor = ct.graytone_to_uint8(canvasColor)
        else:
            raise Exception('{canvasColor} is not a valid color.')
            
        self.canvas = self.canvasColor*np.ones((canvasHeight, canvasWidth), np.uint8) # set up the canvas as an empty string
        self.orientation = None # attribute orienation will be set later
    
    
    def rotate(self, direction):
        """Rotates the pattern counterclockwise (ccw) or clockwise (cw)
        
        :param direction: Direction of the rotation either counterclockwise (ccw) or clockwise (cw)
        :type direction: str
        :returns: None
        :raises: Exception
        
        """
        
        if direction == 'ccw':
            self.canvas = np.rot90(self.canvas, k=1)
        elif direction == 'cw':
            self.canvas = np.rot90(self.canvas, k=-1)
        else: 
            raise Exception('{direction} is not a valid rotation direction.')
            
        return self
            
    def flip_ud(self):
        """Flip pattern up down
        """
        self.canvas = np.flipud(self.canvas)
        
        return self
    
    def flip_lr(self):
        """Flip pattern left right
        """
        self.canvas = np.fliplr(self.canvas)
        
        return self
                            
            
    def invert_color(self):
        """Invert the color of the pattern
        """
        self.canvas = 255-self.canvas
        
        return self
            
    def plot(self):
        """Plot the pattern
        """
        plt.matshow(self.canvas, cmap='gray')
        plt.show()
        
    def save_png(self, filename):
        """Save pattern as png bitmap
        """
        cv2.imwrite(filename, self.canvas)
    
    def __str__(self):
        return str(self.canvas)
    
    def __repr__(self):
        return repr(self.canvas)
            

class Stripes(Pattern):
    """Stripes pattern class
    
    >>> s = Stripes(3, 3, 1, 2, 'horizontal')
    >>> s.name
    'stripes'
    >>> s.canvas
    array([[  0,   0,   0],
           [255, 255, 255],
           [  0,   0,   0]], dtype=uint8)
    >>> s = Stripes(3, 3, 4, 1, 'vertical')
    Traceback (most recent call last):
        ...
    Exception: stripeWidth is too large.
    >>> s = Stripes(3, 3, 1, 4, 'vertical')
    Traceback (most recent call last):
        ...
    Exception: stripePeriod is too large.
    >>> s = Stripes(3, 3, 1, 1, 'vertical', 'blue')
    Traceback (most recent call last):
        ...
    Exception: blue is not a valid color.
    """    
    
    def __init__(self, canvasWidth, canvasHeight, stripeWidth, stripePeriod, orientation, color='black', canvasColor='white'):
    
        super().__init__('stripes', canvasWidth, canvasHeight, canvasColor='white')
        
        # check orientation input
        if orientation in ['vertical', 'horizontal']:
            self.orientation = orientation
        else:
            raise Exception('{orientation} is not a valid orientation.'.format(orientation))
        
        # check if stripe width is not larger than canvas width and height               
        if (orientation == 'vertical') and (stripeWidth <= canvasWidth):
            self.stripeWidth = stripeWidth
        elif (orientation == 'horizontal') and (stripeWidth <= canvasHeight):
            self.stripeWidth = stripeWidth
        else:
            raise Exception('stripeWidth is too large.')
            
        # check that stripe period is not larger than canvas width and height
        if (orientation == 'vertical') and (stripePeriod <= canvasWidth):
                self.stripePeriod = stripePeriod
        elif (orientation == 'horizontal') and (stripePeriod <= canvasHeight):
                self.stripePeriod = stripePeriod
        else:
            raise Exception('stripePeriod is too large.')     
            
            
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        
        
    def __draw__(self):
        
        if self.orientation == 'vertical':
            
            # draw vertical
            for i in range(0, self.canvasWidth,self.stripePeriod):
                self.canvas[:,i:i+self.stripeWidth] = self.color
            
        elif self.orientation == 'horizontal':
            
            # draw horizontal
            for i in range(0, self.canvasHeight,self.stripePeriod):
                self.canvas[i:i+self.stripeWidth,:] = self.color

class StripesDouble(Pattern):
    """Stripes pattern class
    
    >>> s = StripesDouble(3, 3, 1, 2, 'horizontal')
    >>> s.name
    'stripesdouble'
    >>> s.canvas
    array([[  0,   0,   0],
           [255, 255, 255],
           [  0,   0,   0]], dtype=uint8)
    >>> s = Stripes(3, 3, 4, 1, 'vertical')
    Traceback (most recent call last):
        ...
    Exception: stripeWidth is too large.
    >>> s = Stripes(3, 3, 1, 4, 'vertical')
    Traceback (most recent call last):
        ...
    Exception: stripePeriod is too large.
    >>> s = Stripes(3, 3, 1, 1, 'vertical', 'blue')
    Traceback (most recent call last):
        ...
    Exception: blue is not a valid color.
    """    
    
    def __init__(self, canvasWidth, canvasHeight, stripeWidth, stripePeriod, orientation, color='black', canvasColor='white'):
    
        super().__init__('stripesdouble', canvasWidth, canvasHeight, canvasColor='white')
        
        # check orientation input
        if orientation in ['vertical', 'horizontal']:
            self.orientation = orientation
        else:
            raise Exception('{orientation} is not a valid orientation.'.format(orientation))
        
        # check if stripe width is not larger than canvas width and height               
        if (orientation == 'vertical') and (stripeWidth <= canvasWidth):
            self.stripeWidth = stripeWidth
        elif (orientation == 'horizontal') and (stripeWidth <= canvasHeight):
            self.stripeWidth = stripeWidth
        else:
            raise Exception('stripeWidth is too large.')
            
        # check that stripe period is not larger than canvas width and height
        if (orientation == 'vertical') and (stripePeriod <= canvasWidth):
                self.stripePeriod = stripePeriod
        elif (orientation == 'horizontal') and (stripePeriod <= canvasHeight):
                self.stripePeriod = stripePeriod
        else:
            raise Exception('stripePeriod is too large.')     
            
            
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        
        
    def __draw__(self):
        
        if self.orientation == 'vertical':
            
            # draw vertical
            for i in range(self.stripePeriod, self.canvasWidth//2+1,self.stripePeriod*2):
                self.canvas[:self.canvasHeight//2+1,i:i+self.stripeWidth] = self.color
            
            # draw vertical double period
            for i in range(0, self.canvasWidth,self.stripePeriod*2):
                self.canvas[:,i:i+self.stripeWidth] = self.color
            
        elif self.orientation == 'horizontal':
            
            # draw horizontal
            for i in range(self.stripePeriod, self.canvasHeight//2+1, self.stripePeriod*2):
                self.canvas[i:i+self.stripeWidth,:self.canvasWidth//2+1] = self.color
                
            # draw horizontal double period
            for i in range(0, self.canvasHeight, self.stripePeriod*2):
                self.canvas[i:i+self.stripeWidth,:] = self.color

class Checkerboard(Pattern):
    """Checkerboard pattern class
    
    >>> s = Checkerboard(3, 2, 1)
    >>> s.name
    'checkerboard'
    >>> s.canvas
    array([[  0, 255,   0],
           [255,   0, 255]], dtype=uint8)
    >>> s = Stripes(3, 3, 8)
    Traceback (most recent call last):
        ...
    Exception: squareLength is too large.
    """    
    
    def __init__(self, canvasWidth, canvasHeight, squareLength, color='black', canvasColor='white'):
    
        super().__init__('checkerboard', canvasWidth, canvasHeight, canvasColor='white')
        
        # check if stripe width is not larger than canvas width and height               
        if (squareLength <= canvasHeight) and (squareLength <= canvasWidth):
            self.squareLength = squareLength
        else:
            raise Exception('squareLength is too large.')
        
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        
        
    def __draw__(self):
        
        # draw checkerboard
        for counter, i in enumerate(range(0, self.canvasHeight, self.squareLength)):
            # put an offset every other row
            if counter % 2 == 0:
                offset = 0
            else:
                offset = self.squareLength
            for j in range(offset, self.canvasWidth, self.squareLength*2):
                self.canvas[i:i+self.squareLength,j:j+self.squareLength] = self.color


class FractalStripes(Pattern):
    """FractalStripes pattern class
    
    >>> s = FractalStripes(64,64,1,16)
    >>> s.name
    'checkerboard'
    """    
    
    def __init__(self, canvasWidth, canvasHeight, fractalWidthMin, fractalHeight, color='black', canvasColor='white'):
    
        super().__init__('checkerboard', canvasWidth, canvasHeight, canvasColor='white')
        
        # check if fractal width is not larger than canvas width             
        if (fractalHeight <= canvasHeight):
            self.fractalHeight = fractalHeight
        else:
            raise Exception('Fractal Width is too large.')
        # check if fractal with is correct
        if fractalWidthMin >= canvasWidth:
            raise Exception('Fractal with is larger than canvas width')
        else:
            self.fractalWidthMin = fractalWidthMin
        
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 < color < 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{color} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        

        
    def __draw__(self):
        
        period = self.fractalWidthMin*2
        width = self.fractalWidthMin
        for j in range(0, self.canvasHeight, self.fractalHeight):
            for i in range(0, self.canvasWidth, period):
                self.canvas[j:j+self.fractalHeight,i:i+width] = self.color
            period *= 2
            width *= 2


class Corners(Pattern):
    """Corners pattern class
    
    >>> s = Corners(64,64,2,4, 64, 42)
    >>> s.name
    'corners'
    >>> s = Corners(64,64,2,4, 64, 42)
    Traceback (most recent call last):
        ...
    Exception: blue is not a valid color.
    """    
    
    def __init__(self, canvasWidth, canvasHeight, lineWidth, linePeriod, lineLengthMax, lineLengthMin, canvasBorder=0, color='black', canvasColor='white'):
    
        super().__init__('corners', canvasWidth, canvasHeight, canvasColor='white')
        
        self.lineWidth = lineWidth
        self.linePeriod = linePeriod
        self.lineLengthMax = lineLengthMax
        self.lineLengthMin = lineLengthMin
        self.numberOfLines = 5
        self.canvasBorder = canvasBorder
            
            
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        
        
    def __draw__(self):
                
        # draw vertical
        for counter, i in enumerate(range(0, self.linePeriod*self.numberOfLines,self.linePeriod)):
            offset = counter*self.linePeriod
            if counter == 2:
                self.canvas[self.canvasBorder+offset:self.lineLengthMax-self.canvasBorder,
                            self.canvasBorder+i:self.canvasBorder+i+self.lineWidth] = self.color
            else:
                self.canvas[self.canvasBorder+offset:self.lineLengthMin-self.canvasBorder,
                            self.canvasBorder+i:self.canvasBorder+i+self.lineWidth] = self.color
            
        # draw horizontal
        for counter, i in enumerate(range(0, self.linePeriod*self.numberOfLines,self.linePeriod)):
            offset = counter*self.linePeriod
            if counter == 2:
                self.canvas[self.canvasBorder+i:self.canvasBorder+i+self.lineWidth,
                            self.canvasBorder+offset:self.lineLengthMax-self.canvasBorder] = self.color
            else:
                self.canvas[self.canvasBorder+i:self.canvasBorder+i+self.lineWidth,
                            self.canvasBorder+offset:self.lineLengthMin-self.canvasBorder] = self.color
                
class Circle(Pattern):
    """Circle pattern class
    
    >>> s = Circle(63,63,32)
    >>> s.name
    'circle'
    """
    
    def __init__(self, canvasWidth, canvasHeight, circleRadius, color='black', canvasColor='white'):
    
        super().__init__('checkerboard', canvasWidth, canvasHeight, canvasColor='white')
        
        self.circleRadius = circleRadius
        
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()

        
    def __draw__(self):
        
        x0, y0 = (self.canvasWidth//2, self.canvasHeight//2)
        for y in range(-self.circleRadius, self.circleRadius):
            for x in range(-self.circleRadius, self.circleRadius):
                if (x)**2+(y)**2 < self.circleRadius**2:
                    self.canvas[y0+y,x0+x] = self.color


class FractalCircles(Pattern):
    """FractalCircles pattern class
    
    >>> s = FractalCircles(36,96,1,2,5)
    >>> s.name
    'fractalcircles'
    """
    
    def __init__(self, canvasWidth, canvasHeight, circleRadiusMin, fractalMultiplier, numberOfCircles, color='black', canvasColor='white'):
    
        super().__init__('fractalcircles', canvasWidth, canvasHeight, canvasColor)
        
        self.circleRadii = [int(round(circleRadiusMin*fractalMultiplier**n)) for n in range(numberOfCircles)]
        self.numberOfCircles = numberOfCircles
        
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{color} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        
        
    def __draw__(self):
        
        for i in range(self.numberOfCircles):
            
            radius = self.circleRadii[i]
            x0 = self.canvasWidth//2-radius
            y0 = sum(self.circleRadii[0:i])*4
            
            c = Circle(radius*2, radius*2, radius, self.color, self.canvasColor)
            
            self.canvas[y0:y0+radius*2,x0:x0+radius*2] = c.canvas


class SiemensStar(Pattern):
    """SiemensStar pattern class
    
    >>> s = SiemensStar(36,96,1,2,5)
    >>> s.name
    'siemensstar'
    """
    
    def __init__(self, canvasWidth, canvasHeight, circleRadius, numberOfRays, color='black', canvasColor='white'):
    
        super().__init__('siemensstar', canvasWidth, canvasHeight, canvasColor)
        
        self.circleRadius = circleRadius
        self.numberOfRays = numberOfRays
        self.angleSiemensStar = 360./(2*numberOfRays)
        
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{color} is not a valid color.'.format(color))
            
        # draw the stripes
        self.__draw__()
        
        
    def __draw__(self):
        
        x0, y0 = (self.canvasWidth//2, self.canvasHeight//2)
        
        for i in np.arange(0,360,2*self.angleSiemensStar):
            cv2.ellipse(self.canvas, (x0, y0), (self.circleRadius, self.circleRadius), 0, i, i+self.angleSiemensStar, self.color, thickness=-1)
 



class ResolutionTest(Pattern):
    """ResolutionTest pattern class
    
    >>> s = ResolutionTest(36,96,1,2,5)
    >>> s.name
    'resolutiontest'
    """
    
    def __init__(self, canvasWidth, canvasHeight, color='black', canvasColor='white'):
    
        super().__init__('resolutiontest', canvasWidth, canvasHeight, canvasColor)
        
        # check color. either a valid color name or an greyscale integer between 0 and 255
        if (color in ct.valid_color_names) or (isinstance(color, int) and 0 <= color <= 255):
            self.color = ct.graytone_to_uint8(color)
        else:
            raise Exception('{color} is not a valid color.'.format(color))
                
    def add_pattern(self, x0, y0, pattern):
        """Add a pattern to the canvas
        """
        ## TODO: 
        ## - check if x0, y0, widht height fit into canvas
        ## - keep track of occupied space
        
        height, width = pattern.canvas.shape
        self.canvas[y0:y0+height, x0:x0+width] = pattern.canvas
        
        
    def draw_cross_markers(self, markerWidth, markerOffset):
        """Draw cross markers in the corner of the resolution test pattern
        """
        pass
        
    def draw_line_markers(self, markerWidth, markerOffset):
        """Draw line merkers on the left and right side of the test pattern
        """
        
        length = int(self.canvasHeight)
        y0 = 0 #○int(self.canvasHeight)
        
        #draw left marker
        self.canvas[y0:y0+length, markerOffset:markerOffset+markerWidth] = self.color
        
        #draw right marker
        self.canvas[y0:y0+length, self.canvasWidth-markerOffset-markerWidth:self.canvasWidth-markerOffset] = self.color

        return self
        

def test_pattern_20190704():
    """Creates a test pattern with various elements to measure the resolution of the technique
    """
    
    rt = ResolutionTest(2000, 700)
    
    xBorder = 280
    yBorder = 80
    
    # add horizontal stripes
    rt.add_pattern(xBorder, yBorder,  Stripes(100, 100, 1, 4, 'horizontal'))
    rt.add_pattern(200+xBorder, yBorder,  Stripes(100, 100, 1, 6, 'horizontal'))
    rt.add_pattern(400+xBorder, yBorder,  Stripes(100, 100, 1, 8, 'horizontal'))
    rt.add_pattern(600+xBorder, yBorder,  Stripes(100, 100, 1, 10, 'horizontal'))

    # add vertical stripes
    rt.add_pattern(xBorder, 200+yBorder,  Stripes(100, 100, 1, 4, 'vertical'))
    rt.add_pattern(200+xBorder, 200+yBorder,  Stripes(100, 100, 1, 6, 'vertical'))
    rt.add_pattern(400+xBorder, 200+yBorder,  Stripes(100, 100, 1, 8, 'vertical'))    
    rt.add_pattern(600+xBorder, 200+yBorder,  Stripes(100, 100, 1, 10, 'vertical'))    

    # add checkerboards
    rt.add_pattern(xBorder, 400+yBorder, Checkerboard(160, 160, 10))
    rt.add_pattern(xBorder+(710-160)//2, 400+yBorder, Checkerboard(160, 160, 20))
    rt.add_pattern(710+xBorder-160, 400+yBorder, Checkerboard(160, 160, 40))
    
    # add fractal stripes
    fs = FractalStripes(240,240,1,50)
    rt.add_pattern(xBorder+850, yBorder, fs)
    fs.rotate('cw')
    rt.add_pattern(xBorder+850, yBorder+300, fs)
    
    # add circles
    fc = FractalCircles(120,200,1,2,6)
    rt.add_pattern(xBorder+1200, yBorder, fc)
    fc.invert_color()
    fc.flip_ud()
    rt.add_pattern(xBorder+1200+120, yBorder, fc)
    
    # add corners
    fc = Corners(120, 120, 1, 10, 120, 90, 10)
    rt.add_pattern(xBorder+1200, yBorder+440, fc)
    rt.add_pattern(xBorder+1320, yBorder+440, fc.invert_color().rotate('cw'))
    
    fc = Corners(120, 120, 1, 8, 120, 90, 10)
    rt.add_pattern(xBorder+1200, yBorder+300, fc)
    rt.add_pattern(xBorder+1320, yBorder+300, fc.invert_color().rotate('cw'))    
    
    # add markers
    rt.draw_line_markers(2, 100)
    
    return rt

if __name__ == '__main__':
    
#    import doctest
#    doctest.testmod()

#    p = Pattern('test', 10, 10).plot()
#    p.show()
#    plt.show()
#    
#    Test stripes
#    s = Stripes(20,20,2,4,'horizontal')
#    s.plot()
#    plt.show()
#
#    test checkerboard
#    s = Checkerboard(20, 20, 4)
#    s.plot()
#    plt.show()
#    
#    test fractal stripes    
#    s = FractalStripes(64,64,1,16)
#    s.plot()
#    plt.show()

    
#    test fractal stripes    
#    s = Corners(64,64,1,3, 64, 42, 3)
#    s.rotate('cw')
#    s.invert_color()
#    s.save_png('out.png')
#    s.plot()
#    plt.show()

#    test circle    
#    s = Circle(63,63,22)
#    print(s.)
#    s.invert_color()
#    s.save_png('circle_self.png')
#    s.plot()
#    plt.show()

#    test fractal circle s   
#    s = FractalCircles(36,96,1,2,5)
#    s.invert_color()
#    print(s.circleRadii)
#    s.save_png('circle_self.png')
#    s.plot()
#    plt.show()
    
    # test siemens star
#    ss = SiemensStar(100, 100, 50, 8)
#    ss.plot()
#    plt.show()
    
    # test double stripes
    ds = StripesDouble(100, 100, 1, 3, 'horizontal')
    ds.plot()
    plt.show()
    ds.save_png('stripes_double.png')

#    rt = test_pattern_20190704()
    
    # plotting
#    rt.plot()
#    plt.show()
    
#    rt.save_png('resolutionTest_2000x100.png')