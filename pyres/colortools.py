#!/usr/bin/env python3
"""
Copyright (c) (2019) Samuel Zimmermann <samuel.zimmermann@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


def graytone_to_uint8(grayTone):
    """From a graytone name return the corresponding greyscale value
    
    >>> graytone_to_uint8('white')
    255
    >>> graytone_to_uint8('black')
    0
    >>> graytone_to_uint8('gray')
    127
    >>> graytone_to_uint8('blue')
    Traceback (most recent call last):
        ...
    Exception: blue is not a valid gray tone
    """
    if isinstance(grayTone, str):
        if grayTone == 'white':
            return 255
        elif grayTone == 'black':
            return 0
        elif grayTone == 'gray':
            return 127
        else:
            raise Exception('{} is not a valid gray tone'.format(grayTone))
    elif 0<=grayTone<=255:
        return grayTone
    else:
        raise Exception('{} is not a valid gray tone'.format(grayTone))

valid_color_names = ['black', 'gray', 'white']
        
if __name__ == "__main__":
    
    import doctest
    doctest.testmod()
